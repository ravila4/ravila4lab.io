---
title: About Me
subtitle: I am a biologist, data scientist, photographer, and artist based in San Diego, California.
description: I am a biologist, data scientist, photographer, and artist based in El Paso, Texas.
featured_image: /images/about/profile.jpg
---

![](/images/about/profile.jpg)

I live in between disciplines. From my day-to-day work in the field of biotech, where I combine software development, statistics, and data visualization towards uncovering insights from biological data; to my passion for photography and art in its many forms. More than anything, I enjoy creating new things.

I was born in Mexico, and grew up in El Paso, Texas, a border city that taught me to take pride in my heritage. I learned to appreciate the beauty of living in the intersection between different cultures, and it is the stories, and memories growing up in this environment that nurtured my passion for street and portrait photography. Science found its way into my art at an early age - starting with a fascination for animals, dinosaurs, and insects - to date, nature photography is still one of my favorite subjects.

I received a B.S. in Biochemistry and M.S. in Bioinformatics from the University of Texas at El Paso, then spent some time as an intern at a large pharmaceutical company in Philadelphia, following which, I began my search for the next big adventure!
Currently, I am living in San Diego, CA, where I am enjoying the nice weather, and working on new scientific and artistic projects.

If you would like to know more about my research work, you can check out my other site: [ravilabio.info](https://ravilabio.info/)

---

## Contact

Have a Question? I would love to hear from you.

You can Email me for any pricing, collaboration, or commission inquiries.

Email: ravila@ravilart.com
