---
title: 'Entangled: A Painting Collaboration'
date: 2018-09-19 00:00:00
description: 'The process of painting "Entangled", a story of a whale and a jellyfish.' 
featured_image: '/images/posts/entangled/whale-cover.jpg'
---

This is a little art project I worked on in collaboration with my girlfriend [@yuejiaoxian](https://instagram.com/yuejiaoxian) during the stress-free days before the start of the semester.
She wanted to draw a whale, while I was partial toward something with a little more tentacles, so we settled on combining both ideas.


<div class="gallery" data-columns="2">
	<img src="/images/posts/entangled/whale2.jpg">
	<img src="/images/posts/entangled/whale1.jpg">
	<img src="/images/posts/entangled/whale3.jpg">
	<img src="/images/posts/entangled/whale8.jpg">
	<img src="/images/posts/entangled/whale4.jpg">
	<img src="/images/posts/entangled/whale5.jpg">
	<img src="/images/posts/entangled/whale6.jpg">
	<img src="/images/posts/entangled/whale9.jpg">
</div>


Drawing with another person can be a very fun experience. We both had a hand in every part of the drawing - although she ended up drawing most of the jellyfish, while I focused on the whale.

The materials were mostly watercolor and pen, with a little bit of India ink thrown in for shadows, and salt grains for texture on the whale. We signed the piece “Raxy”, from of our initials... I know it's cheesy... We’ll work on it.

![](/images/posts/entangled/whale.jpg)

This will be the second artwork that we have made together[^1], and it’s the first one that we get to keep! Hopefully we’ll get do some more.

---

**Update:** We later decided that we wanted to change the background for this piece. We made them float in space! You can see the finished work [here](/art/entangled).

---

![](/images/posts/entangled/curandera.jpg)

[^1]: “La Curandera.” — Was the first drawing that my girlfriend and I made together. It was a gift for a friend’s mom. 

