---
title: 'Making Zongzi'
date: 2020-07-01 00:00:00
description: 'Making zongzi, or "rice tamales" to celebrate the Chinese Dragon Boat Festival'  
featured_image: '/images/posts/tamales/zongzi-cover.jpg'
---

Last week [@yuexiaoxian](https://www.instagram.com/yuejiaoxian/) and I made zongzi (粽子) to celebrate the Chinese Dragon Boat Festival.

Zongzi are one of my favorite Chinese dishes. They are made of sticky rice, wrapped in bamboo leaves, and can be stuffed with either chicken, pork, or sweet red beans, among other things.

<div class="gallery" data-columns="2">
        <img src="/images/posts/tamales/zongzi03-2.jpg">
        <img src="/images/posts/tamales/zongzi05-2.jpg">
</div>

I call them "rice tamales", because they remind me of our Mexican dish, which is made with stuffed corn flour wrapped in corn or banana leaves. Like Mexican tamales, Chinese tamales also come in salty and sweet varieties.


<div class="gallery" data-columns="4">
        <img src="/images/posts/tamales/zongzi04-2.jpg">
        <img src="/images/posts/tamales/zongzi09-2.jpg">
        <img src="/images/posts/tamales/zongzi10-3.jpg">
        <img src="/images/posts/tamales/zongzi11-2.jpg">
        <img src="/images/posts/tamales/zongzi12-2.jpg">
</div>

The process involves making a cone out of bamboo leaves, and filling it with rice and stuffing.

Wraping the tamales with the bamboo leaves takes a lot of skill. That's why I was in charge of taking pictures, and tying the finished ones with string. 😛
