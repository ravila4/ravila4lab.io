---
title: 'Chalk the Block 2018'
date: 2018-10-16 00:00:00
description: ""
featured_image: '/images/posts/ctb2018/chalk-fav-2.jpg'
---

**Chalk the Block** is a three-day-long public arts festival in **El Paso, TX**, that usually takes place in the first weekend of October. It features many artists, live performances, and vendors. I have been attending this event every year, for about eight years now, because of the many great opportunities for taking beautiful pictures. Nevertheless, this year was my first time participating as a chalk artist.

![](/images/posts/ctb2018/thankyouursh.jpg)

The drawing that we submitted for the event is a watercolor piece by my sister: [@tired.fish](https://www.instagram.com/tired.fish/). My sister, my girlfriend, and I spent the entire morning creating a chalk rendition, which was surprisingly difficult, given that we have little experience with chalk pastels!

Here are some pictures of my two minions working:

<div class="gallery" data-columns="4">
	<img src="/images/posts/ctb2018/chalking-1.jpg">
	<img src="/images/posts/ctb2018/chalking-2.jpg">
	<img src="/images/posts/ctb2018/chalking-3.jpg">
	<img src="/images/posts/ctb2018/chalking-4.jpg">
	<img src="/images/posts/ctb2018/chalking-5.jpg">
	<img src="/images/posts/ctb2018/chalking-6.jpg">
	<img src="/images/posts/ctb2018/chalk-fav-1.jpg">
	<img src="/images/posts/ctb2018/chalk-fav-2.jpg">
</div>

Imagine our surprise when we found out that we won second place in the “Emerging Artist” category!

![](/images/posts/ctb2018/chalk-award.jpg)

Here are some other pictures of the event:

<div class="gallery" data-columns="4">
	<img src="/images/posts/ctb2018/ctb-1.jpg">
	<img src="/images/posts/ctb2018/ctb-2.jpg">
	<img src="/images/posts/ctb2018/ctb-3.jpg">
	<img src="/images/posts/ctb2018/ctb-4.jpg">
	<img src="/images/posts/ctb2018/ctb-5.jpg">
</div>

And this is me, playing with giant chalk:

![](/images/posts/ctb2018/ctb-6.jpg)

Every year is different, and equally exciting, so if you're ever in El Paso during the season, make sure to stop by!
