---
title: 'Manayunk Arts Festival'
date: 2018-07-28 00:00:00
description: 
featured_image: '/images/posts/manayunk/girl-and-her-dog.jpg'
---

For my first blog post, I want to share some street pictures that I took at the Manayunk Arts Festival in Philadelphia. I am currently staying in Phoenixville, Pennsylvania for an internship, and this was one of my first times exploring the city of Philadelphia.

![](/images/posts/manayunk/girl-and-her-dog.jpg)

The plan was simple. I rode my bicycle more than 18 miles along the Schuykyll trail (I thought riding all the way to Philly would be easy). Nevertheless, it was through my stubbornness that I stumbled into this event. It was worth it. The photograph at the top of this post is one of my favorites that I’ve taken in a long time.

There was something very simple and beautiful about capturing the image of the girl feeding her dog under the table, amid all the buzz and commotion. I remember the mild panic when I dropped to my knees, fumbling to get my manual lens in focus, and the joy when I noticed that the people walking around me had stopped too, curious to see what I was photographing, and were now admiring it too.

I leave you with a few more photos:

<div class="gallery" data-columns="2">
	<img src="/images/posts/manayunk/jazz-band.jpg">
	<img src="/images/posts/manayunk/jewlery.jpg">
	<img src="/images/posts/manayunk/at-the-bar.jpg">
	<img src="/images/posts/manayunk/manayunk-rest.jpg">
</div>

