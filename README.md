# Ricardo Avila's Art Portfolio

## Getting Started
1. Clone this repository and `cd` into it
2. Install Jekyll and Bundler: `gem install jekyll bundler:1.17.3`
3. Install required packages: `bundle install --path vendor/bundle`
4. Start server: `bundle exec jekyll serve`

## Creating New Posts


## Continuous Integration
When a commit is made to master, the website is built and deployed.
The file **.gitlab-ci.yml** contains the build configuration. 
