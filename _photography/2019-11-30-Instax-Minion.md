---
title: 'Instax Minion'
subtitle: 'Capturing Moments with an Instant Camera'
description: 'Photographs on Instax Mini film.'
featured_image: '/images/photography/instax/instax-cover.jpg'
---

Here is a collection of shots taken with the Instax Minion - 
a fun little camera that reminds me to have fun, value the moment, and not take my 
photography gear too seriously.

---

<div class="gallery" data-columns="4">
	<img src="/images/photography/instax/instax01.jpg">
	<img src="/images/photography/instax/instax05.jpg">
	<img src="/images/photography/instax/instax03.jpg">
	<img src="/images/photography/instax/instax02.jpg">
	<img src="/images/photography/instax/instax04.jpg">
	<img src="/images/photography/instax/instax11.jpg">
	<img src="/images/photography/instax/instax06.jpg">
	<img src="/images/photography/instax/instax10.jpg">
	<img src="/images/photography/instax/instax08.jpg">
	<img src="/images/photography/instax/instax07.jpg">
	<img src="/images/photography/instax/instax09.jpg">
</div>

