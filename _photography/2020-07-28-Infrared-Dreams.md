---
title: 'Infrared Dreams'
subtitle: 'Surreal Landscapes in Infrared Light'
description: 'A collection of infrared photos taken in San Diego, California.'
featured_image: '/images/photography/infrared/infrared-cover.jpg'
---

A collection of infrared photos taken in San Diego, California.

---

<div class="gallery" data-columns="2">
	<img src="/images/photography/infrared/infrared01.jpg">
	<img src="/images/photography/infrared/infrared02.jpg">
</div>

