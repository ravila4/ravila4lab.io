---
title: 'Pink Dreams'
subtitle: 'Infrared portraits of Yuejiao'
description: 'Experimenting with infrared portrait photography.'
featured_image: '/images/photography/ir_portraits/ir-cover.jpg'
---


<div class="gallery" data-columns="2">
	<img src="/images/photography/ir_portraits/ir1.jpg">
	<img src="/images/photography/ir_portraits/ir2.jpg">
</div>

