---
title: 'The City of Philadelphia'
subtitle: 'Getting to Know the City of Brotherly Love'
description: 'Photographing the city of brotherly love.'
featured_image: '/images/photography/philly/phil-cover.jpg'
---

I first arrived in Philadelphia in the summer of 2018, when I spent three months doing a summer internship at a pharmaceutical company. Little did I know, that I would end up coming back to this city, and spending almost a full year photographing its streets and landscapes.

Learning to see Philadelphia is coming to embrace its many contrasts. Long humid summers and cold winter days. The perfect juxtaposition of the classical and the modern. Wild forest meeting the city's stone and concrete. The city itself is an amazing struggle between human and nature.

---

<div class="gallery" data-columns="2">
	<img src="/images/photography/philly/phil1.jpg">
	<img src="/images/photography/philly/phil12.jpg">
	<img src="/images/photography/philly/phil2.jpg">
	<img src="/images/photography/philly/phil9.jpg">
	<img src="/images/photography/philly/phil3.jpg">
	<img src="/images/photography/philly/phil4.jpg">
	<img src="/images/photography/philly/phil5.jpg">
	<img src="/images/photography/philly/phil6.jpg">
	<img src="/images/photography/philly/phil7.jpg">
	<img src="/images/photography/philly/phil8.jpg">
	<img src="/images/photography/philly/phil10.jpg">
	<img src="/images/photography/philly/phil11.jpg">
	<img src="/images/photography/philly/phil13.jpg">
	<img src="/images/photography/philly/phil16.jpg">
</div>

