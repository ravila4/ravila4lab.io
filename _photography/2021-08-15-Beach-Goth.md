---
title: 'Beach Goth'
subtitle: 'Goths go to the beach too...'
description: 'Polaroids of goths at the beach.'
featured_image: '/images/photography/beach_goth/beach-goth-cover.jpg'
---


<div class="gallery" data-columns="4">
	<img src="/images/photography/beach_goth/beach-goth-1.jpg">
	<img src="/images/photography/beach_goth/beach-goth-2.jpg">
	<img src="/images/photography/beach_goth/beach-goth-3.jpg">
	<img src="/images/photography/beach_goth/beach-goth-4.jpg">
	<img src="/images/photography/beach_goth/beach-goth-5.jpg">
	<img src="/images/photography/beach_goth/beach-goth-6.jpg">
	<img src="/images/photography/beach_goth/beach-goth-7.jpg">
	<img src="/images/photography/beach_goth/beach-goth-8.jpg">
</div>

