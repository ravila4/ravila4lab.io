---
title: "A Bug's World"
subtitle: 'Photos of Insects (and Arachnids)'
description: A collection of insect and spider macro photography.
featured_image: '/images/photography/macro/macro-cover.jpg'
---

This is a collection of photographs taken between 2012 and 2015 in El Paso, Texas. Many of these insects were found in the Centennial Desert Gardens of the University of Texas at El Paso. 

---

<div class="gallery" data-columns="2">
	<img src="/images/photography/macro/macro-1.jpg">
	<img src="/images/photography/macro/macro-2.jpg">
	<img src="/images/photography/macro/macro-3.jpg">
	<img src="/images/photography/macro/macro-4.jpg">
	<img src="/images/photography/macro/macro-5.jpg">
	<img src="/images/photography/macro/macro-6.jpg">
	<img src="/images/photography/macro/macro-7.jpg">
	<img src="/images/photography/macro/macro-8.jpg">
	<img src="/images/photography/macro/macro-9.jpg">
	<img src="/images/photography/macro/macro-10.jpg">
</div>
