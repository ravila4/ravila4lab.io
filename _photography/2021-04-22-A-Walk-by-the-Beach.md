---
title: 'A Walk by the Beach'
subtitle: 'Celebrating our 4th anniversary'
description: 'Polaroids by the beach'
featured_image: '/images/photography/beach_polaroids/beach-sunflower-cover.jpg'
---

These Polaroids were taken on our dating aniversary. We took a walk by the beach with the SX-70 camera and a sunflower.

---

<div class="gallery" data-columns="3">
	<img src="/images/photography/beach_polaroids/beach-sunflower1.jpg">
	<img src="/images/photography/beach_polaroids/beach-sunflower2.jpg">
	<img src="/images/photography/beach_polaroids/beach-sunflower3.jpg">
	<img src="/images/photography/beach_polaroids/beach-sunflower4.jpg">
	<img src="/images/photography/beach_polaroids/beach-sunflower5.jpg">
	<img src="/images/photography/beach_polaroids/beach-sunflower6.jpg">
</div>

