---
title: 'People of Philly'
subtitle: 'Photographing Strangers in Philadelphia'
description: Portraits of strangers in the streets of Philadelphia.
featured_image: '/images/photography/philly/philly-portrait-cover.jpg'
---

The beauty of street photography is in what it teaches us about empathy. To photograph a stranger is to take something intimate, and be reminded of our shared humanity.

This project documents my street photography journey in the beautiful city of Philadelphia.

---
<div class="gallery" data-columns="2">
	<img src="/images/photography/philly/philly-portrait-040.jpg">
	<img src="/images/photography/philly/philly-portrait-070.jpg">
	<img src="/images/photography/philly/philly-portrait-010.jpg">
	<img src="/images/photography/philly/philly-portrait-020.jpg">
	<img src="/images/photography/philly/philly-portrait-021.jpg">
	<img src="/images/photography/philly/philly-portrait-050.jpg">
	<img src="/images/photography/philly/philly-portrait-030.jpg">
	<img src="/images/photography/philly/philly-portrait-080.jpg">
	<img src="/images/photography/philly/philly-portrait-060.jpg">
	<img src="/images/photography/philly/philly-portrait-090.jpg">
	<img src="/images/photography/philly/philly-portrait-100.jpg">
</div>
